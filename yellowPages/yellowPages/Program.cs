﻿using System;
using System.Collections.Generic;

namespace yellowPages
{
    class Program
    {
        // Collection of Persons, in a List.
        public static List<Person> collectionPerson = new List<Person>();

        // Fill List with Persons.
        private static void FillList(List<Person> list)
        {
            list.Add(new Person("Elspeth", "Orozco"));
            list.Add(new Person("Louise", "Mcintosh"));
            list.Add(new Person("Kaidan", "Bowler"));
            list.Add(new Person("Madelaine", "snow"));
            list.Add(new Person("Kameron", "Palacios"));
            list.Add(new Person("Arandeep", "snow"));
            list.Add(new Person("Shanae", "Henderson"));
            list.Add(new Person("Leonidas", "Meadows"));
        }

        // Get user input from console as lowercase string.
        private static string GetUserInput()
        {
            string userInput = Console.ReadLine().ToLower();
            return userInput;
        }
        // Searching the List of Persons for a Person with the search name.
        private static void FindPerson(List<Person> list, string searchString)
        {
            foreach (Person item in list)
            {
                string findPerson = "";
                findPerson += item.FirstName;
                findPerson += " ";
                findPerson += item.LastName;

                if (findPerson.ToLower().Contains(searchString))
                {
                    Console.WriteLine(findPerson);
                }
            }
        }
        // Prints welcome text to console for better user experience.
        private static void PrintWelcomeMessage()
        {
            Console.WriteLine("############################################################");
            Console.WriteLine("###             Welcome to the yellow pages.             ###");
            Console.WriteLine("###  Type a name, or parts of the name you try to find.  ###");
            Console.WriteLine("############################################################");
        }

        static void Main(string[] args)
        {
            bool isRunning = true;
            FillList(collectionPerson);

            Console.WriteLine(collectionPerson.Count);

            while (isRunning)
            {
                try
                {
                    PrintWelcomeMessage();
                    string myString = GetUserInput();
                    FindPerson(collectionPerson, myString);

                    Console.WriteLine("Did you find the person you were looking for? please type yes if you did, or type anything to try again.");

                    if (GetUserInput() == "yes")
                    {
                        isRunning = false;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception", ex.Message);
                }
            }

        }
    }
}
