﻿using System;
using System.Collections.Generic;
using System.Text;

namespace yellowPages
{
    class Person
    {
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public int Telephone { get; set; }
    public int Height { get; set; }

        public Person(string FirstName, string LastName)
        {
            this.FirstName = FirstName;
            this.LastName = LastName;
        }

        public Person() { }
    
        
    }
}
